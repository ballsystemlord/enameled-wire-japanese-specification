# Enameled wire Japanese specification


## Name
Enameled wire Japanese specification.

## Description
A translation of the online Japanese enameled wire specification.

## Support
This project is techncally completed, so no support is not applicable. That being said, use the issue tracker to talk.

## Roadmap
I'd imagine that there is an American standard for this, but I can't find it. There are too many irrelevant search results.
Having additional documentation about enameled wire in here is also a good idea. Feel free to add some.
I would be nice to redo the main document in latex or something so that it looks better.

## Contributing
Just create pull request in the issue tracker. You must also note in here the license of any work you upload, of course.

## Authors and acknowledgment
https://www.kuramo.co.jp/catalog/magnet/magnet_wire.pdf
^ The original Japanese document.
www.onlinedoctranslator.com , for their pdf translation service.
ImTranslator browser add-on, for their help with fixing what the above website messed up in the translation.
David Niklas <- Me.

## License
Fair use. I do not own the original document and I only edited the translation for clarity.

## Project status
Complete?
